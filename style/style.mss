Map {
  background-color: @land-color;
}

@water-color: @blue1;    // Blue 2 from the GNOME HIG palette
@land-color: mix(@light2, @light3, 50%);     // Light 2 from the GNOME HIG palette

@standard-halo-radius: 1;
@standard-halo-fill: rgba(255,255,255,0.6);
